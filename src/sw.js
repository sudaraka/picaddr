/* src/sw.js: serviceworker source
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

const CACHE_KEY = 'picaddr'
    , CACHE_ID = `${CACHE_KEY}-${REVISION}`
    , ASSETS = [ '/picaddr/', ...serviceWorkerOption.assets ]

    , addToCache = (id, assets) => caches
        .open(id)
        .then(cache => cache.addAll(assets))
        .catch(console.error)

    , removeUnusedCaches = id => caches
        .keys()
        .then(keys => keys.forEach(key => {
          if(key.match(`${CACHE_KEY}-`) && CACHE_ID !== key) {
            return caches.delete(key)
          }
        }))

    , fromCacheOrNetwork = (id, request) => caches
        .match(request)
        .then(cached_response => {
          if(cached_response) {
            return cached_response
          }
          else {
            return fetch(request)
              .then(network_response => {
                caches
                  .open(CACHE_ID)
                  .then(cache => cache.add(request, network_response))

                return network_response.clone()
              })
              .catch(console.error)
          }
        })


// SW install: cache all tracked assets
self.addEventListener('install', e => e.waitUntil(addToCache(CACHE_ID, ASSETS)))

// SW activate: remove all unused caches
self.addEventListener('activate', e => e.waitUntil(removeUnusedCaches(CACHE_ID)))

// SW fetch: respond from cache & use network as fallback
self.addEventListener('fetch', e => e.respondWith(fromCacheOrNetwork(CACHE_ID, e.request)))
