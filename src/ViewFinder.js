/* src/ViewFinder.js: View finder & image capture library
 *
 * Copyright 2019, 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

'use strict'

function initViewFinder() {
  let attached_parent = null

  const event_map = {}

      , video = document.createElement('video')

      , container = document.createElement('div')

      , appendTo = parent => {
          attached_parent = parent

          attached_parent.appendChild(container)

          return _internals
        }

      , play = stream => {
          video.srcObject = stream
          video.play()

          return _internals
        }

      // Create camera button bar DOM elements
      , buttons = () => {
          const div = document.createElement('div')

          div.innerHTML = `
            <div class="buttons">
              <button type="button" class="close">
                &times;
              </button>
              <button type="button" class="capture">
                <img src="assets/img/button-green.svg" alt="" />
              </button>
            </div>
          `.trim()

          return div.firstChild
        }

      , buttonNode = buttons()

      , on = (tag, f) => {
          if('function' === typeof f) {
            event_map[tag] = f
          }

          return _internals
        }

      , handle_event = (tag, ...params) => {
          if('function' === typeof event_map[tag]) {
            event_map[tag](...params)
          }
        }

      , close = () => {
          attached_parent.removeChild(container)

          handle_event('close')
        }

      , _internals = { appendTo, close, on, play }

  // Handle close button click
  buttonNode
    .querySelector('.close')
    .addEventListener('click', close)

  // Handle capture button click
  buttonNode
    .querySelector('.capture')
    .addEventListener('click', () => {
      const video_width = video.videoWidth
          , video_height = video.videoHeight

      if(video_height && video_width) {
        const canvas = document.createElement('canvas')
            , context = canvas.getContext('2d')

        canvas.width = video_width
        canvas.height = video_height

        context.drawImage(video, 0, 0, video_width, video_height)

        handle_event
          ( 'capture'
          , { ..._internals, 'dataUrl': canvas.toDataURL('image/jpeg', .8) }
          )
      }
    })


  // Add elements to container
  container.setAttribute('id', 'viewFinder')
  container.appendChild(video)
  container.appendChild(buttonNode)

  return _internals
}

module.exports = initViewFinder()
