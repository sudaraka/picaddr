# Makefile: main build script
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY
# This is free software, and you are welcome to redistribute it and/or modify
# it under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

APP_ENV ?=dev

-include ./env/${APP_ENV}.mk
-include ./env/base.mk

WEBPACK_FLAGS=--output-path=${DIST_DIR}
WEBPACK_FLAGS:=${WEBPACK_FLAGS} --define VERSION="\"`git describe --tags --abbrev=0 2>/dev/null`\""

ifeq ($(APP_ENV), dev)
	WEBPACK_FLAGS:=${WEBPACK_FLAGS} -d
	WEBPACK_FLAGS:=${WEBPACK_FLAGS} --define REVISION="\"`cat /dev/urandom|head |sha1sum|cut -c -6`\""
else
	WEBPACK_FLAGS:=${WEBPACK_FLAGS} -p
	WEBPACK_FLAGS:=${WEBPACK_FLAGS} --define REVISION="\"`git rev-parse --short HEAD`\""
endif

all:
	@echo "USAGE:"
	@echo "  make build		: build application"
	@echo "  make clean		: remove temporary files & build artifacts"
	@echo "  make clean-all	: clean & remove Node.js, Elm modules"
	@echo "  make run-http	: run development server"


.PHONY: build clean clean-all deploy run-http

build:
	npx webpack \
		${WEBPACK_FLAGS} \
		--hide-modules \
		--progress

run-http:
	npx webpack-dev-server \
		${WEBPACK_FLAGS} \
		--host ${HTTP_HOST} \
		--port ${HTTP_PORT} \
		--hot \
		--content-base ${DIST_DIR} \
		--open

deploy: build
ifeq ($(APP_ENV), dev)
	@echo
	@echo -e '\e[1;31mDepolyment only allowed in production mode.\e[m'
	@echo '  run "APP_ENV=prod make deploy"'
	@echo
else
	git ftp push \
		--insecure \
		--syncroot `basename ${DIST_DIR}` \
		--user ${DEPLOY_USER} \
		$(filter-out $@,$(MAKECMDGOALS)) \
		${DEPLOY_URL}
endif

clean:
	$(RM) -r ${DIST_DIR}

clean-all: clean
	$(RM) -r ${BASE_DIR}/node_modules
	$(RM) -r ${BASE_DIR}/elm-stuff
